### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 9fa7af8c-fcd1-4ca7-8e70-46eadbf90d05
using Markdown

# ╔═╡ a0fcc337-f98a-4ca7-9298-cbf100a1a442
using InteractiveUtils

# ╔═╡ 95a1eb28-df51-44f1-85c7-45b5216e068d
using PlutoUI

# ╔═╡ f68bb269-ee25-4d9d-a08a-7187846e0ae0
using AStarSearch

# ╔═╡ 31a1860b-8775-4be3-9bf6-b404bde922eb
md"## Assignment 2 Problem 1"

# ╔═╡ 7d57ee0c-185f-4f76-8bcb-fefd560f692d
md"##### Defining Action"

# ╔═╡ 75d6a094-a6b6-431b-aaa2-393e01d6156e
struct Action
	name::String
	cost::Int64
end

# ╔═╡ bf5d1c92-aaa1-4993-a666-bdf542ef9661
mu = Action("move up", 1)

# ╔═╡ 48092cba-7c02-4ef3-8a91-ad6849df50ac
md = Action("move down", 1)

# ╔═╡ 823194e2-6a5e-402e-bc4e-170dbd057c0f
me = Action("move east", 2)

# ╔═╡ 09431b9b-04f8-4268-b835-758fe09b1347
mw = Action("move west", 2)

# ╔═╡ 1f9a720a-4daa-46e2-86ef-fcf7d5548654
co = Action("collect", 5)

# ╔═╡ 0a68d3c3-d943-4c38-a713-b588d63dd61c
struct Office
	OfficeName::String
	Items::Int64
end

# ╔═╡ 82f0622a-2a2f-4f56-936e-a1c1e464d875
struct Position
	Verbal::Office
end

# ╔═╡ 0981b931-b315-4e82-854e-85aff7e95b57
function Collect(Office, Action)
	if Action == co
		Office.Items - 1
	end
end

# ╔═╡ 32156644-0965-4632-833e-5bec999c7505
W = Office("First Office", 1)

# ╔═╡ 5c0962f4-f55a-4762-a253-3bbae8d2d7e4
C1 = Office("Second Office", 3)

# ╔═╡ 99ee9dac-3254-42e1-b7a0-3a83115d47e8
C2 = Office("Third Office", 2)

# ╔═╡ e0bd3253-da5c-488e-bf91-08a1affffc01
E = Office("Fourth Office", 1)

# ╔═╡ 0b385f24-5335-4917-8ac2-255b04d6c5b2
Start = Position(W)

# ╔═╡ 3d736cbd-fd63-4300-abea-25e3d6c75530
Actions = Dict(Start => (mu, C1))

# ╔═╡ 1e6e8777-c691-4871-9486-ce482b326dc5
push!(Actions, Start => (me, C2))

# ╔═╡ d74db98c-9399-4411-b1cb-bcf34f80ee91
push!(Actions, Start => (mw, W))

# ╔═╡ 64160a24-d3aa-41e0-810d-b3ad3a3fead0
push!(Actions, Start => (co, C1))

# ╔═╡ 5a2fc582-9ef8-449c-99a0-a442e9a06995
function astar(start, isgoal, getneighbours, heuristic;
               cost = defaultcost, timeout = Inf, hashfn = defaulthash, maxcost = Inf, maxdepth = Inf)
  starttime = time()
  startheuristic = heuristic(start)
  startcost = zero(cost(start, start))
  nodetype = typeof(start)
  costtype = typeof(startcost)
  startnode = Node{nodetype, costtype}(start, zero(Int32), startcost, startheuristic, startheuristic, nothing, nothing)
  bestnode = startnode
  starthash = hashfn(start)

  closedset = Set{typeof(starthash)}()
  openheap = MutableBinaryHeap(Base.By(nodeorderingkey), Node{nodetype, costtype}[])
  starthandle = push!(openheap, startnode)
  startnode.heaphandle = starthandle
  opennodedict = Dict(starthash=>startnode)

  while !isempty(openheap)
    if time() - starttime > timeout
      return AStarResult{nodetype, costtype}(:timeout, reconstructpath(bestnode), bestnode.g, length(closedset), length(openheap))
    end

    node = pop!(openheap)
    nodehash = hashfn(node.data)
    delete!(opennodedict, nodehash)

    if isgoal(node.data)
      return AStarResult{nodetype, costtype}(:success, reconstructpath(node), node.g, length(closedset), length(openheap))
    end

    push!(closedset, nodehash)

    if node.h < bestnode.h
      bestnode = node
    end

    neighbours = getneighbours(node.data)

    for neighbour in neighbours
      neighbourhash = hashfn(neighbour)
      if neighbourhash in closedset
        continue
      end

      gfromthisnode = node.g + cost(node.data, neighbour)

      if gfromthisnode > maxcost
        continue
      end

      if node.depth > maxdepth
        continue
      end

      if neighbourhash in keys(opennodedict)
        neighbournode = opennodedict[neighbourhash]
        if gfromthisnode < neighbournode.g
          neighbournode.depth = node.depth + one(Int32)
          neighbournode.g = gfromthisnode
          neighbournode.parent = node
          neighbournode.f = gfromthisnode + neighbournode.h
          update!(openheap, neighbournode.heaphandle, neighbournode)
        end
      else
        neighbourheuristic = heuristic(neighbour)
        neighbournode = Node{nodetype, costtype}(neighbour, node.depth + one(Int32), gfromthisnode, neighbourheuristic, gfromthisnode + neighbourheuristic, node, nothing)
        neighbourhandle = push!(openheap, neighbournode)
        neighbournode.heaphandle = neighbourhandle
        push!(opennodedict, neighbourhash => neighbournode)
      end
    end
  end

  return AStarResult{nodetype, costtype}(:nopath, reconstructpath(bestnode), bestnode.g, length(closedset), length(openheap))
end

# ╔═╡ 1498716b-8c2c-474b-aa08-c2cc673dde8a
abstract type AbstractAStarSearch{T} end

# ╔═╡ 0dc28996-f976-42a9-8b5f-2ab03a8207ba
isgoal(astarsearch::AbstractAStarSearch{T}, current::T, goal::T) where T = current == goal

# ╔═╡ 4fcab473-b753-43f4-9695-507f653dd2e2
neighbours(astarsearch::AbstractAStarSearch{T}, current::T) where T = throw("neighbours not implemented! Implement it for your AbstractAStarSearch subtype!")

# ╔═╡ a14340c2-d84c-453f-828b-fffd76d5dbf0
heuristic(astarsearch::AbstractAStarSearch{T}, current::T, goal::T) where T = throw("heuristic not implemented! Implement it for your AbstractAStarSearch subtype!")

# ╔═╡ 5cd23e72-b6aa-49e6-aa0c-d69509eec3d0
cost(astarsearch::AbstractAStarSearch{T}, current::T, neighbour::T) where T = defaultcost(current, neighbour)

# ╔═╡ 1947838a-cf16-457b-a021-47a498b24bc5
#function search(aastarsearch::AbstractAStarSearch{T}, start::T, goal::T; timeout = Inf, maxcost = Inf, maxdepth = Inf) where T
 # _isgoal(x::T) = isgoal(aastarsearch, x, goal)
  #_neighbours(x::T) = neighbours(aastarsearch, x)
  #_heuristic(x::T) = heuristic(aastarsearch, x, goal)
  #_cost(current::T, neighbour::T) = cost(aastarsearch, current, neighbour)
  #_hashfn(x::T) = hash(x)
  #return astar(start, _isgoal, _neighbours, _heuristic, cost = _cost, hashfn = _hashfn; timeout, maxcost, maxdepth)
#end

# Cell order:
# ╠═d45787f2-b17e-11eb-3a9e-1dadd509823b
# ╠═e6673bba-6361-44f9-b257-d29e19e5c996
# ╠═b5f98ef3-d67e-467a-b466-0dc5dec09f15
# ╠═6c418973-d55e-4c8f-879f-5f600f71f1a5
# ╠═4249ab5f-79fa-4cc6-ae28-ea6c7b0b0b7a
# ╠═13a185bc-2256-4442-8c2c-dc9acdaa7caa
# ╠═b6390c7d-d7a9-479f-a09a-60c625f14094
# ╠═4afa8a0d-0877-4de6-9948-63b4aad6f1c3
# ╠═febc2824-aadb-4eff-92cf-82f38caf2c01
# ╠═fe8cffd9-b234-4da5-8f98-02a3dc647aa1
# ╠═7d885cf7-fb31-436c-814f-f86c60252667
# ╠═590d6fb6-1998-4c20-b6f7-a648d152e406
# ╠═9af2bf18-95c1-45a7-bac1-6b04f24caa66
# ╠═c32c40b9-748b-4275-9948-6d8c7bb7e0b8
# ╠═46550753-a86c-41d5-a3df-aafd86278bda
# ╠═949bc457-0c6f-49c5-abf6-1a81482ab1a5
# ╠═bc8f0178-fd59-45a2-a2cc-52307485bfeb
# ╠═39a0e930-9c8a-4ba2-a581-0a45995fffe4
# ╠═da025b87-dda4-449e-9bd6-66514f39b51a
# ╠═ac6ab451-846d-4d30-987e-8ea0bd1beef8
# ╠═8eee0df6-7407-47e8-b1d9-7fb2a4b52fd9
# ╠═dad276d7-7e0c-4ef9-b559-2aeb757ba210
# ╠═f1aee843-5ed4-48a4-bdf9-a8e154ea112f
# ╠═09922478-fe7f-44e4-81f5-882802734a73
# ╠═06353ed6-ce38-4c73-84b5-c565779eec16
# ╠═e7f0213f-2a93-43b1-b729-a350ced75071
# ╠═c3e82520-2b7a-4bb9-87b1-e9927e51ebee
# ╠═37e64082-209f-4acb-97f0-3d98af83674a
# ╠═722b4dba-6fed-42f6-9b01-6b8a68aee9c2
# ╠═cf0979fa-ba36-4ef7-9e69-41c63752c090
# ╠═d7e27b51-b721-41f1-9ef2-af13e337e7c9
# ╠═c4d5b9a6-623f-481d-b40e-49a1e92338c2

# ╔═╡ 217e3eb9-7915-4909-a258-f68ac0d62eff
begin
using Pkg
	Pkg.add("AStarSearch")

end

# ╔═╡ e61b614d-b4ca-4cfe-9fa4-1adcd1b0b0ac
using Pkg

# ╔═╡ 2c68eaa9-89df-45b5-acd5-6176393b4f37
import Pkg

# ╔═╡ Cell order:
# ╠═31a1860b-8775-4be3-9bf6-b404bde922eb
# ╠═9fa7af8c-fcd1-4ca7-8e70-46eadbf90d05
# ╠═a0fcc337-f98a-4ca7-9298-cbf100a1a442
# ╠═e61b614d-b4ca-4cfe-9fa4-1adcd1b0b0ac
# ╠═95a1eb28-df51-44f1-85c7-45b5216e068d
# ╠═2c68eaa9-89df-45b5-acd5-6176393b4f37
# ╠═217e3eb9-7915-4909-a258-f68ac0d62eff
# ╠═f68bb269-ee25-4d9d-a08a-7187846e0ae0
# ╠═7d57ee0c-185f-4f76-8bcb-fefd560f692d
# ╠═75d6a094-a6b6-431b-aaa2-393e01d6156e
# ╠═bf5d1c92-aaa1-4993-a666-bdf542ef9661
# ╠═48092cba-7c02-4ef3-8a91-ad6849df50ac
# ╠═823194e2-6a5e-402e-bc4e-170dbd057c0f
# ╠═09431b9b-04f8-4268-b835-758fe09b1347
# ╠═1f9a720a-4daa-46e2-86ef-fcf7d5548654
# ╠═0a68d3c3-d943-4c38-a713-b588d63dd61c
# ╠═82f0622a-2a2f-4f56-936e-a1c1e464d875
# ╠═0981b931-b315-4e82-854e-85aff7e95b57
# ╠═32156644-0965-4632-833e-5bec999c7505
# ╠═5c0962f4-f55a-4762-a253-3bbae8d2d7e4
# ╠═99ee9dac-3254-42e1-b7a0-3a83115d47e8
# ╠═e0bd3253-da5c-488e-bf91-08a1affffc01
# ╠═0b385f24-5335-4917-8ac2-255b04d6c5b2
# ╠═3d736cbd-fd63-4300-abea-25e3d6c75530
# ╠═1e6e8777-c691-4871-9486-ce482b326dc5
# ╠═d74db98c-9399-4411-b1cb-bcf34f80ee91
# ╠═64160a24-d3aa-41e0-810d-b3ad3a3fead0
# ╠═5a2fc582-9ef8-449c-99a0-a442e9a06995
# ╠═1498716b-8c2c-474b-aa08-c2cc673dde8a
# ╠═0dc28996-f976-42a9-8b5f-2ab03a8207ba
# ╠═4fcab473-b753-43f4-9695-507f653dd2e2
# ╠═a14340c2-d84c-453f-828b-fffd76d5dbf0
# ╠═5cd23e72-b6aa-49e6-aa0c-d69509eec3d0
# ╠═1947838a-cf16-457b-a021-47a498b24bc5
